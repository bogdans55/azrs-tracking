### Projekat iz predmeta Alati za razvoj softvera

U okviru ovog projekta odabrano je 10 korisnih softverskih alata koji su primenjeni u izradi projekta [StudySphere](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/StudySphere).

Alati koji su primenjeni
- Git
- GDB
- ClangFormat
- ClangTidy
- Docker
- Git hooks
- Skript jezici (bash, batch)
- Build sistemi (qmake, CMake)
- Doxygen
- Gitlab CI